#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name)
	: Polygon(type, name)
{
	// Meaning the points are making a line.
	if ((a.getX() == b.getX() && b.getX() == c.getX()) || (a.getY() == b.getY() && b.getY() == c.getY()))
	{
		throw std::invalid_argument("The points entered create a line.");
	}
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}

Triangle::~Triangle() {}

double Triangle::getArea() const
{
	/*
	       |     Ax(By-Cy) + Bx(Cy-Ay) + Cx(Ay-By)     |
	area = |-------------------------------------------|
	       |                    2                      |
	*/
	double sum = 0;
	sum += _points[0].getX() * (_points[1].getY() - _points[2].getY());
	sum += _points[1].getX() * (_points[2].getY() - _points[0].getY());
	sum += _points[2].getX() * (_points[0].getY() - _points[1].getY());
	return abs(sum / 2);
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
