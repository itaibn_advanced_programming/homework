#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:
	enum Shapes { circle, arrow, triangle, rectangle };
	enum Colors { red, green, blue };
	enum MainMenu { add_shape_choice, modify_shape_choice, delete_all_shapes, exit_choice };
	enum ModifyShapeMenu { move_shape, get_details, remove_shape };

	Menu();
	~Menu();

	void addCircle(vector<Shape*>& shapes);
	void addArrow(vector<Shape*>& shapes);
	void addTriangle(vector<Shape*>& shapes);
	void addRectangle(vector<Shape*>& shapes);

	void add_shape(vector<Shape*>& shapes);

	unsigned short displayChoicesMenu();

	unsigned short displayShapeObjectsMenu();

	void showAllShapes(vector<Shape*>& shapes);

	void showModifyShapeMenu(vector<Shape*>& shapes, unsigned short index);

	void moveShapeMenu(vector<Shape*>& shapes, unsigned short index);

	void removeShape(vector<Shape*>& shapes, unsigned short index, bool delete_from_shapes);

	void deleteAllShapes(vector<Shape*>& shapes);

private: 
	void refresh();

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

