#include "Rectangle.h"
#include <exception>


myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name)
: Polygon(type, name)
{
	// Meaning the points are making a line.
	if (width == 0 || length == 0)
	{
		throw std::invalid_argument("Length or Width can't be 0.");
	}
	_points.push_back(a);
	_points.push_back(Point(a.getX() + abs(length), a.getY() + abs(width)));
}

myShapes::Rectangle::~Rectangle()
{}

double myShapes::Rectangle::getArea() const
{
	// Get the third point so calculating the distance will be duable.
	Point thirdPoint(_points[0].getX(), _points[1].getY());
	return abs(_points[0].distance(thirdPoint) * _points[1].distance(thirdPoint));
}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


