#pragma once
#include "Polygon.h"

class Arrow: public Shape
{
public:
	Arrow(const Point& a, const Point& b, const string& type, const string& name);
	~Arrow();

	// There's a need to enter an opposite Y when moving an arrow.
	virtual void move(const Point& other);

	virtual double getPerimeter() const ;
	virtual double getArea() const ;

	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

private:
	vector<Point> _points;
};

