#include "Circle.h"

Circle::Circle(const Point & center, double radius, const string & type, const string & name)
	: Shape(name, type), _center(center), _radius(radius) 
{}

Circle::~Circle() {}

const Point& Circle::getCenter() const 
{
	return _center;
}

double Circle::getRadius() const
{
	return _radius;
}

double Circle::getArea() const
{
	return _radius * _radius * PI;
}

double Circle::getPerimeter() const
{
	return 2 * PI * _radius;
}

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	board.draw_circle(_center.getX(), _center.getY(), _radius, BLUE, 100.0f).display(disp);

	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_circle(_center.getX(), _center.getY(), _radius, BLACK, 100.0f).display(disp);

}

void Circle::move(const Point& other)
{
	_center+= other;
}
