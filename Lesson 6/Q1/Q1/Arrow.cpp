#include "Arrow.h"


Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name)
	: Shape(type, name)
{
	_points.push_back(a);
	_points.push_back(b);
}


Arrow::~Arrow()
{
}

void Arrow::move(const Point & other)
{
	Point newPoint(other.getX(), other.getY() * -1);
	
	for (int i = 0; i < _points.size(); i++)
	{
		_points[i] += newPoint;
	}
}

double Arrow::getPerimeter() const
{
	return _points[0].distance(_points[1]);
}

double Arrow::getArea() const
{
	return 0.0;
}

void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), RED, 100.0f).display(disp);
}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


