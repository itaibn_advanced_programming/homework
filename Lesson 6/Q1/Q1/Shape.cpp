#include "Shape.h"

Shape::Shape(const string & name, const string & type) : _name(name), _type(type)
{}

void Shape::printDetails() const
{
	cout << _type.c_str() << "\t" << _name.c_str() << "\t" << getArea() << "\t" << getPerimeter() << endl; 
}


string Shape::getType() const 
{ 
	return _type; 
}

string Shape::getName() const 
{ 
	return _name; 
}

